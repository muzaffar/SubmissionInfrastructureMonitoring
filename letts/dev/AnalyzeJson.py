#!/usr/bin/python
# Add sched eff by Nego also
import sys
import htcondor
import classad
import json
import time
import datetime
import glob
import os
import math
from collections import defaultdict

# Open json file
try :
  INPUTFILE=sys.argv[1]
except IndexError :
  INPUTFILE=max(glob.glob('./*.js'),key=os.path.getctime)
try :
  json_data=open(INPUTFILE,"r")
  data = json.load(json_data)
except IOError :
  print >> sys.stderr, "Unable to open file: "+INPUTFILE
  sys.exit(1)
except ValueError :
  print >> sys.stderr, "Unable to decode json file: "+INPUTFILE
  sys.exit(2)

# Get the time the file was produced
os.environ["TZ"]="GMT"
EpochTime=int(str(INPUTFILE).split(".")[-2])

# autocluster Ads
TotalAutoClusters=0
AutoClusters=defaultdict(int)
AutoClustersBySchedd=defaultdict(int)
AnalysisAutoClusters=defaultdict(int)
JobPressure=defaultdict(int)
AnalysisJobPressure=defaultdict(int)
for datum in data :
  try :
    Type=datum["MyType"]
    if Type !=  "AutoCluster" : continue
  except KeyError :
    continue
  TotalAutoClusters+=1
  AutoClustersBySchedd[datum["Scheduler"]]+=1
  # Determine if an analysis or HammerCloud job
  try :
    datum["CRAB_UserHN"]
    is_analysis='true'
  except KeyError :
    is_analysis='false'
  try :
    # Get Job Pressure and Autoclusters by Site
    DESIRED_Sites=str(datum["DESIRED_Sites"])
    try :
      Pressure=int(datum["JobCount"])*int(datum["RequestCpus"])
    except ValueError :
      Pressure=int(datum["JobCount"])*int(datum["OriginalCpus"])
    for Site in DESIRED_Sites.split(",") :
      AutoClusters[Site]+=1
      JobPressure[Site]+=Pressure
      if is_analysis is 'true' :
        AnalysisAutoClusters[Site]+=1
        AnalysisJobPressure[Site]+=Pressure
  except KeyError :
    continue

  
# Get schedd Ads: 
SCHEDD={}
for datum in data :
  try :
    Type=datum["MyType"]
    if Type !=  "Scheduler" : continue
  except KeyError :
    continue
  try : 
    SCHEDD[datum["Name"]]=[
      datum["CollectorHost"],
      datum["MaxJobsRunning"],
      (int(datum["TotalRunningJobs"])-int(datum["TotalLocalJobsRunning"])),
      (int(datum["TotalIdleJobs"])-int(datum["TotalLocalJobsIdle"])),
      datum["TotalHeldJobs"],
      datum["RecentResourceRequestsSent"], 
      datum["RecentDaemonCoreDutyCycle"]]
  except KeyError :
    continue
# Some totals from schedd Ads
TOTAL=[0,0,0,0,0,0]
for schedd in sorted(SCHEDD.keys()) :
  TOTAL[2]+=SCHEDD[schedd][2]
  TOTAL[3]+=SCHEDD[schedd][3]
  TOTAL[4]+=SCHEDD[schedd][4]
  TOTAL[5]+=SCHEDD[schedd][5]
TotalRunningJobs=TOTAL[2]
TotalIdleJobs=TOTAL[3]
TotalHeldJobs=TOTAL[4]
TotalRRRLs=TOTAL[5]

# Pilot Ads by Site:
Analysis=defaultdict(int)
Production=defaultdict(int)
Unclaimed=defaultdict(int)
Retire=defaultdict(int)
Starve=defaultdict(int)
Ioslots=defaultdict(int)
RemainingIdle=defaultdict(int)
ClaimedStates={}
JobTime=defaultdict(int)
for datum in data :
  try :
    Type=datum["MyType"]
    if Type !=  "Machine" : continue
  except KeyError :
    continue
  try :
    Cpus=datum["CPUs"]
    if Cpus == 0 : continue
  except KeyError :
    continue
  try :
    State=datum["State"]
    Activity=datum["Activity"]
    Site=datum["GLIDEIN_CMSSite"]
  except KeyError :
    continue
  if State == "Unclaimed" :
    Unclaimed[Site]+=Cpus 
    try :
      GLIDEIN_ToRetire=datum["GLIDEIN_ToRetire"]
    except KeyError :
      GLIDEIN_ToRetire=datum["MyCurrentTime"]+1
    if GLIDEIN_ToRetire < datum["MyCurrentTime"] :
      Retire[Site]+=Cpus 
    elif datum["Memory"] < 2048*Cpus :
      Starve[Site]+=Cpus 
    else :
      try : 
        if datum["Repackslots"] >= Cpus :
          Ioslots[Site]+=datum["Repackslots"]
          Cpus-=datum["Repackslots"]
        elif datum["Ioslots"] >= Cpus :
          Ioslots[Site]+=datum["Ioslots"]
          Cpus-=datum["Ioslots"] 
      except KeyError :
        pass
      RemainingIdle[Site]+=Cpus 
  else :
    try :
      JobId=datum["GlobalJobId"]
      ScheddName=str(JobId).split('#')[0]
      ThisJobTime=datum["MyCurrentTime"]-datum["JobStart"]
      JobTime[ScheddName]+=ThisJobTime
      if "crab" in JobId or "login.uscms.org" in JobId : 
        Analysis[Site]+=Cpus
      # add Tier-0 workflows here
      else :
        Production[Site]+=Cpus
    except KeyError :
      continue
    try :   
      ClaimedStates[Cpus,int(datum["Memory"]),Site]+=1
    except KeyError :
      ClaimedStates[Cpus,int(datum["Memory"]),Site]=1

AnalysisPercent=defaultdict(int)
TotalCoresSite=defaultdict(int)
SchedEff=defaultdict(int)
ProductionJobPressure=defaultdict(int)
AnalysisPercentByNego=defaultdict(int)
TotalCoresByNego=defaultdict(int)
Keys=list(set(Analysis.keys()+Production.keys()))
for Site in sorted(Keys): 
  AnalysisPercent[Site]=Analysis[Site]/float(
    Analysis[Site]+Production[Site])*100.
  TotalCoresSite[Site]=Analysis[Site]+Production[Site]+Unclaimed[Site]
  try :
    SchedEff[Site]=100.*(1.-float(RemainingIdle[Site])/TotalCoresSite[Site])
  except ZeroDivisionError :
    SchedEff[Site]=0.
  ProductionJobPressure[Site]=JobPressure[Site]-AnalysisJobPressure[Site]
  if "T1_" in Site : 
    AnalysisPercentByNego["NEGOTIATORT1@vocms0815.cern.ch"]+=Analysis[Site]
    TotalCoresByNego["NEGOTIATORT1@vocms0815.cern.ch"]+=TotalCoresSite[Site]
  elif "T2_US_" in Site :
    AnalysisPercentByNego["NEGOTIATORUS@vocms0815.cern.ch"]+=Analysis[Site]
    TotalCoresByNego["NEGOTIATORUS@vocms0815.cern.ch"]+=TotalCoresSite[Site]
  elif "T2_CH_CERN" not in Site and "T3_CH_Volunteer" not in Site :
    AnalysisPercentByNego["vocms0815.cern.ch"]+=Analysis[Site]
    TotalCoresByNego["vocms0815.cern.ch"]+=TotalCoresSite[Site]

try :
  AnalysisPercentByNego["NEGOTIATORT1@vocms0815.cern.ch"]/=float(
    TotalCoresByNego["NEGOTIATORT1@vocms0815.cern.ch"])/100.
except ZeroDivisionError :
  AnalysisPercentByNego["NEGOTIATORT1@vocms0815.cern.ch"]=0.
try :
  AnalysisPercentByNego["NEGOTIATORUS@vocms0815.cern.ch"]/=float(
    TotalCoresByNego["NEGOTIATORUS@vocms0815.cern.ch"])/100.
except ZeroDivisionError :
  AnalysisPercentByNego["NEGOTIATORUS@vocms0815.cern.ch"]=0.
try :
  AnalysisPercentByNego["vocms0815.cern.ch"]/=float(
    TotalCoresByNego["vocms0815.cern.ch"])/100.
except ZeroDivisionError :
  AnalysisPercentByNego["vocms0815.cern.ch"]=0.
try :
  AnalysisPercentByNego["vocms0820.cern.ch"]=Analysis["T2_CH_CERN"]/float(
    TotalCoresSite["T2_CH_CERN"])*100.
except ZeroDivisionError :
  AnalysisPercentByNego["vocms0820.cern.ch"]=0.
try :
  AnalysisPercentByNego["vocms0840.cern.ch"]=Analysis["T3_CH_Volunteer"]/float(
    TotalCoresSite["T3_CH_Volunteer"])*100.
except ZeroDivisionError :
  AnalysisPercentByNego["vocms0840.cern.ch"]=0.

# Get Negotiator ads: Name, Pool, Cycle Length
NEGO={}
for datum in data :
  try :
    Type=datum["MyType"]
    if Type !=  "Negotiator" : continue
  except KeyError :
    continue
  try : 
    CycleLength=int(datum["LastNegotiationCycleDuration0"])
  except KeyError :
    continue
  Name=str(datum["Name"])
  PoolName=Name.split("@")[-1]
  NegoName=Name.split("@")[0]
  if PoolName == "NEGOTIATOR" :
    PoolName="vocms0840.cern.ch"
  if NegoName == PoolName :
    NegoName="NEGOTIATOR"
  NEGO[Name]=[NegoName,PoolName,CycleLength,AnalysisPercentByNego[Name]]

# All done collecting Ads!

#NEGO[Name]=[NegoName,PoolName,CycleLength,AnaPct]

#SCHEDD[Name]=[CollectorHost,MaxJobsRunning,TotalRunningJobs,TotalIdleJobs,
#       TotalHeldJobs.RecentResourceRequestsSent,RecentDaemonCoreDutyCycle]

#TotalRunningJobs
#TotalIdleJobs
#TotalHeldJobs
#TotalRRRLs
#TotalAutoClusters

#Analysis[Site]
#Production[Site]
#Unclaimed[Site]
#Retire[Site]
#Starve[Site]
#Ioslots[Site]
#RemainingIdle[Site]
#ClaimedStates[Site]
#AutoClusters[Site]
#AnalysisAutoClusters[Site]
#JobPressure[Site]
#AnalysisJobPressure[Site]

TotalCoresClaimed=sum(Analysis.values())+sum(Production.values())
TotalCores=TotalCoresClaimed+sum(Unclaimed.values())
TotalIdleJobsCoresEst=int(round(int(TotalIdleJobs*TotalCoresClaimed/float(
  TotalRunningJobs)),-4))
TotalSchedEff=100.*(1.-sum(RemainingIdle.values())/float(TotalCores))
CoresPerJob=TotalCoresClaimed/float(TotalRunningJobs)
RRRLsPerAutoCluster=TotalRRRLs/float(TotalAutoClusters)
EquivSeconds=int(20.*60./RRRLsPerAutoCluster)
TotalAvgRunningJobTime=sum(JobTime.values())/60./TotalRunningJobs

print "<HTML>"
print "<BODY>"
print "<PRE>"
print "HTCondor Pools Monitoring",
print time.strftime('%Y-%m-%d %H:%M:%S %Z', time.gmtime(EpochTime))
print 
print "Headline Numbers:"
print
print "Total CPU Cores                 :",TotalCores
print "Scheduling Efficiency           :",round(TotalSchedEff,1),"%"
print 
print "Claimed States (Cpu,Memory,Site):",len(ClaimedStates.keys())
print "Idle Job Autoclusters           :",TotalAutoClusters
print "Total Claimed Cores             :",TotalCoresClaimed
print "Total Queued Cores (Est.)       :",TotalIdleJobsCoresEst
print "Total RRRLs per AutoCluster     :",round(RRRLsPerAutoCluster,1),
print "        Effective Period: ",EquivSeconds,"s"
print "Average Cores per Running Job   :",round(CoresPerJob,1),
print "         Dynamic Slots: ",int(TotalCoresClaimed/CoresPerJob)
print "Average Running Job Time (m)    :",int(TotalAvgRunningJobTime)
print
print "Negotiators:"
print
print ("%-20s,%-20s,%10s,%10s" % ("Name","Pool","Cycle(s)","AnaShare"))
for key in sorted(NEGO.keys()):
  print ("%-20s,%-20s,%10i,%9.1f%%" % (NEGO[key][0],NEGO[key][1],NEGO[key][2],NEGO[key][3]))
print
print "Schedulers:"
print 
print("%-31s,%-31s,%10s,%10s,%10s,%10s,%10s,%10s,%10s,%10s" % ("Schedd Name",
  "CollectorHost", "MaxJobs", "RunJobs", "IdleJobs", "HeldJobs", 
  "RunTime(m)", "RRRLs", "AutoClust","DutyCycle"))
for schedd in sorted(SCHEDD.keys()) :
  try : 
    AverageJobRunTime=JobTime[schedd]/float(SCHEDD[schedd][2])/60.
  except ZeroDivisionError :
    AverageJobRunTime=0
  print("%-31s,%-31s,%10i,%10i,%10i,%10i,%10i,%10i,%10i,%9i%%" % (
    schedd, 
    str(SCHEDD[schedd][0]).split(":")[0], 
    SCHEDD[schedd][1], 
    SCHEDD[schedd][2], 
    SCHEDD[schedd][3],
    SCHEDD[schedd][4],
    AverageJobRunTime,
    SCHEDD[schedd][5],
    AutoClustersBySchedd[schedd],
    100.*float(SCHEDD[schedd][6])))
print("%-31s,%-31s,%10s,%10i,%10i,%10i,%10i,%10i,%10i," % (
  "TOTAL"," "," ",TOTAL[2],TOTAL[3],TOTAL[4],TotalAvgRunningJobTime,TOTAL[5],
  sum(AutoClustersBySchedd.values())))
print
print "Site Table:"
print
print("%-31s,%9s,%9s,%9s,%9s,%9s,%9s,%9s,%9s,%9s,%9s," % 
  ("Site", "ProdCpus", "AnaCpus", "AnaShare", "RetireCpu", 
  "StarveCpu", "Ioslots", "IdleCpus", "SchedEff", "ProdPress", 
  "AnaPress"))
for Site in sorted(Keys): 
  print("%-31s," % Site ),
  print ("%8d," % Production[Site] ),
  print ("%8d," % Analysis[Site] ),
  print ("%7.1f%%," % AnalysisPercent[Site] ),
  print ("%8d," % Retire[Site] ),
  print ("%8d," % Starve[Site] ),
  print ("%8d," % Ioslots[Site] ),
  print ("%8d," % RemainingIdle[Site] ),
  print ("%7.1f%%," % SchedEff[Site] ),
  print ("%8d," % ProductionJobPressure[Site] ),
  print ("%8d," % AnalysisJobPressure[Site] ),
  print
print("%-31s," % "TOTAL" ),   
print ("%8d," % sum(Production.values()) ),
print ("%8d," % sum(Analysis.values()) ),
AverageAnalysisPercent=100.*sum(Analysis.values())/float(
  sum(Analysis.values())+sum(Production.values()))
print ("%7.1f%%," % AverageAnalysisPercent ),
print ("%8d," % sum(Retire.values()) ),
print ("%8d," % sum(Starve.values()) ),
print ("%8d," % sum(Ioslots.values()) ),
print ("%8d," % sum(RemainingIdle.values()) ),
print ("%7.1f%%," % TotalSchedEff ),
print ("%8s," % "" ),
print ("%8s," % "" ),
print 
print "</PRE>"
print "</BODY>"
print "</HTML>"

sys.exit()