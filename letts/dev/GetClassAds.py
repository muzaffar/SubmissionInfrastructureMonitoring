#!/usr/bin/python
import sys,os
import htcondor,classad
import json
import time,datetime
import logging

logging.basicConfig(level=logging.DEBUG,format='%(asctime)s - %(message)s',
  datefmt='%d-%m-%y %H:%M:%S')
log = logging.getLogger(__name__)
pid=os.getpid()

pilot_projection=["State","Activity","SlotType","CPUs","Memory","Disk",
  "MyCurrentTime","GLIDEIN_CMSSite","GLIDEIN_ToDie","GLIDEIN_ToRetire",
  "GLIDEIN_Job_Max_Time","GLIDEIN_MAX_Walltime","GLIDECLIENT_Name",
  "DaemonStartTime","GLIDEIN_Schedd","GlobalJobId","Repackslots",
  "DetectedRepackslots","Ioslots","DetectedIoslots","GLIDECLIENT_Group",
  "GLIDEIN_COLLECTOR_NAME","JobStart","MyType"]

nego_projection=["Name","LastNegotiationCycleDuration0","MyCurrentTime",
  "MyType"]

schedd_projection=["Name","MaxJobsRunning","TotalRunningJobs",
  "TotalLocalJobsRunning","TotalIdleJobs","TotalLocalJobsIdle",
  "TotalHeldJobs","ServerTime","RecentDaemonCoreDutyCycle",
  "CollectorHost","RecentResourceRequestsSent","Machine","CondorPlatform",
  "AddressV1","MyAddress","CondorVersion","MyType"]

# Open and initiate a json file to output the results
OUTPUTFILE="HTCondorClassAds."+str(int(time.time()))+".js"
file=open(OUTPUTFILE,'w')
print >>file, "["

# Arguments are a list of central managers of pools
CentralManagerMachines=sys.argv[1:]
if len(CentralManagerMachines) == 0 :
  CentralManagerMachines=["vocms0815.cern.ch","vocms0820.cern.ch",
    "vocms0840.cern.ch"]

#Only count each schedd once
Schedd_Already_Analyzed=[]
for CentralManagerMachine in CentralManagerMachines :
  Collector=htcondor.Collector(CentralManagerMachine)
  Collector9620=htcondor.Collector(CentralManagerMachine+":9620")
  log.debug("%s - %s: Starting", pid, CentralManagerMachine)

  schedd_ads=Collector9620.query(htcondor.AdTypes.Schedd,'true',
    schedd_projection)
  # Filter schedd ads manually for now until the above constraint works 
  # Does not catch purely flocking schedd's like CMSConnect and MIT
  tmp_schedd_ads=[]
  for schedd_ad in schedd_ads :
    if schedd_ad["Name"] not in Schedd_Already_Analyzed :
      Schedd_Already_Analyzed.append(schedd_ad["Name"])
      tmp_schedd_ads.append(schedd_ad)
  schedd_ads=tmp_schedd_ads
  log.debug("%s - %s: Schedd query", pid, CentralManagerMachine)

  # Negotiator query - failover to other central manager if no ClassAds
  nego_ads=Collector.query(htcondor.AdTypes.Negotiator,"true",nego_projection)
  if len(nego_ads) == 0 :
    All_CentralManagers=list(schedd_ads[0]["CollectorHost"].split(","))
    for CentralManager in All_CentralManagers :
      if CentralManagerMachine not in CentralManager :
        OtherCollector=htcondor.Collector(CentralManager.split(":")[0])
        nego_ads=OtherCollector.query(htcondor.AdTypes.Negotiator,"true",
          nego_projection)
  log.debug("%s - %s: Nego query", pid, CentralManagerMachine)

  # Pilot query
  startd_ads=Collector.query(htcondor.AdTypes.Startd,"true",pilot_projection)
  log.debug("%s - %s: Startd query", pid, CentralManagerMachine)
  
# Now look at job ads
  autocluster_ads=[]
  for schedd_ad in schedd_ads :
    try :
      iterator=htcondor.Schedd(schedd_ad).xquery(requirements="JobStatus=?=1",
        opts=htcondor.QueryOpts.AutoCluster)
      for ad in iterator :
        ad.update({"MyType": "AutoCluster", "Scheduler": schedd_ad["Name"]})
        autocluster_ads.append(ad)
    except RuntimeError :
      print "ERROR connecting to schedd", schedd_ad['Name']
    log.debug("%s - %s: Querying schedd: %s", pid, CentralManagerMachine,
      schedd_ad['Name'])
  log.debug("%s - %s: Autoclusters", pid, CentralManagerMachine)

# Output the results to the json file and close
  for ad in schedd_ads+startd_ads+nego_ads+autocluster_ads :
    print >>file, ad.printJson(),
    print >>file, ","
  log.debug("%s - %s: Writing", pid, CentralManagerMachine)
print >> file, "{\n  \"MyType\" : \"Junk\"\n}\n]"
file.close()

# Verify that the output file is parsable json
try :
  json_data=open(OUTPUTFILE,"r")
except IOError :
  print >> sys.stderr, "Unable to open file: "+OUTPUTFILE
  sys.exit(1)
try :
  data = json.load(json_data)
  print >> sys.stderr, "json output verified: "+OUTPUTFILE
except ValueError :
  print >> sys.stderr, "Unable to decode json file: "+OUTPUTFILE
  sys.exit(2)
json_data.close()
log.debug("%s - %s: Json conversion", pid, CentralManagerMachine)

sys.exit()