#!/usr/bin/python
import sys
import htcondor
import classad
import json
import time
import datetime
import logging
import os
from ConvertClassAdtoJson import convert_ClassAd_to_json

logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(message)s', datefmt='%d-%m-%y %H:%M:%S')
log = logging.getLogger(__name__)
pid=os.getpid()

try :
  CentralManagerMachine=sys.argv[1]
except IndexError :
  CentralManagerMachine="cmsgwms-collector-global.cern.ch"

pilot_projection=["State","Activity","SlotType","CPUs","Memory","Disk",
  "MyCurrentTime","GLIDEIN_CMSSite","GLIDEIN_ToDie","GLIDEIN_ToRetire",
  "GLIDEIN_Job_Max_Time","GLIDEIN_MAX_Walltime","GLIDECLIENT_Name",
  "DaemonStartTime","GLIDEIN_Schedd","GlobalJobId","Repackslots",
  "DetectedRepackslots","Ioslots","DetectedIoslots","GLIDECLIENT_Group",
  "MyType"]

nego_projection=["Name","LastNegotiationCycleDuration0","MyCurrentTime",
  "MyType"]

schedd_projection=["Name","MaxJobsRunning","TotalRunningJobs",
  "TotalIdleJobs","TotalHeldJobs","ServerTime","MyType",
  "RecentDaemonCoreDutyCycle"]

Collector=htcondor.Collector(CentralManagerMachine)
Collector9620=htcondor.Collector(CentralManagerMachine+":9620")

log.debug("%s - %s: Starting", pid, CentralManagerMachine)
# Negotiator query
nego_ads=Collector.query(htcondor.AdTypes.Negotiator,"true",nego_projection)

log.debug("%s - %s: Nego query", pid, CentralManagerMachine)
# Schedd query
schedd_ads=Collector9620.query(htcondor.AdTypes.Schedd,"true",schedd_projection)

log.debug("%s - %s: Schedd query", pid, CentralManagerMachine)
# Pilot query
startd_ads=Collector.query(htcondor.AdTypes.Startd,"true",pilot_projection)

log.debug("%s - %s: Startd query", pid, CentralManagerMachine)
# Idle AutoCluster non-blocking xquery's - projections do not work.
autocluster_ads=[]
projection_aux=['Machine','CondorPlatform', 'Name', 'AddressV1', 'MyAddress', 'CondorVersion']
schedd_ads2 =  Collector.query(htcondor.AdTypes.Schedd, "true", projection_aux)
for schedd_ad in schedd_ads2:
  log.debug("%s - %s: Querying schedd: %s", pid, CentralManagerMachine, schedd_ad['Name'])
  try :
    iterator=htcondor.Schedd(schedd_ad).xquery(requirements="JobStatus=?=1",opts=htcondor.QueryOpts.AutoCluster)
    for ad in iterator:
	    autocluster_ads.append(ad)
  except RuntimeError :
    print "ERROR connecting to schedd", schedd_ad['Name']
log.debug("%s - %s: Autocluster query", pid, CentralManagerMachine)
# Output the results to json file

timestamp=int(time.time())
OUTPUTFILE=str(CentralManagerMachine)+"."+str(timestamp)+".js"
file=open(OUTPUTFILE,'w')

print >> file, "["
for record in startd_ads+nego_ads+schedd_ads+autocluster_ads :
  convert_ClassAd_to_json(record,file)
print >> file, "{\n  \"MyType\" : \"Junk\"\n}\n]"
file.close()

log.debug("%s - %s: Writting", pid, CentralManagerMachine)
# Verify that the output file is parsable json
try :
  json_data=open(OUTPUTFILE,"r")
except IOError :
  print >> sys.stderr, "Unable to open file: "+OUTPUTFILE
  sys.exit(4)
try :
  data = json.load(json_data)
  print >> sys.stderr, "json output verified: "+OUTPUTFILE
except ValueError :
  print >> sys.stderr, "Unable to decode json file: "+OUTPUTFILE
  sys.exit(5)
json_data.close()

log.debug("%s - %s: Json conversion", pid, CentralManagerMachine)
sys.exit()
