#!/bin/sh

# cd into the directory where the script is so we can use relative paths
script_path=$(dirname $(readlink -f $0))
cd $script_path

# This is the path were the html files need to be put
export HTMLPATH="/eos/project/c/cms-htcondor-monitor/www/letts"

ls /eos/project/c/cms-htcondor-monitor/www/letts &> /dev/null
if [ $? != 0 ]; then
    kinit siserv@CERN.CH -k -t ../siserv.keytab 2>> log2.err
    /usr/bin/eosfusebind 2>> log2.err
fi


python GetClassAds.py vocms0840.cern.ch >> $PWD/log.err 2>&1
python AnalyzeJson.py > volunteer.html.tmp 2>> $PWD/log.err
mv volunteer.html.tmp  $HTMLPATH/volunteer.html

python GetClassAds.py vocms0820.cern.ch >> $PWD/log.err 2>&1
python AnalyzeJson.py > cern.html.tmp 2>> $PWD/log.err
mv cern.html.tmp  $HTMLPATH/cern.html

python GetClassAds.py vocms0809.cern.ch >> $PWD/log.err 2>&1
python AnalyzeJson.py > itb.html.tmp 2>> $PWD/log.err
mv itb.html.tmp  $HTMLPATH/itb.html 

python GetClassAds.py vocms0815.cern.ch >> $PWD/log.err 2>&1
python AnalyzeJson.py > global.html.tmp 2>> $PWD/log.err
mv global.html.tmp  $HTMLPATH/global.html 

#TODO
# Acummulate N files and then compress them
gzip *.js
mv *.js.gz ./logs

