from CRABClient.UserUtilities import config
import os

# Change the following three parameters (if needed)
siteName = os.environ['SITE']
arch = os.environ['SCRAM_ARCH'].split('_')[0]

numJobs = 10

config = config()

# Figure the project directory name out (add the progressive number)
i=0
#requestName = '%s_testITBClassad' % siteName
requestName = 'test_%s_%s' % (siteName, arch)
while os.path.isdir("crab_%s_%s" % (requestName ,i) ):
    i+=1
config.General.requestName   = requestName + '_' + str(i)

config.JobType.pluginName  = 'PrivateMC'
config.JobType.psetName    = 'pset.py'

config.Data.splitting = 'EventBased'
config.Data.unitsPerJob = 100 # This is the parameter to change if you want longer/shorter jobs
config.Data.totalUnits = config.Data.unitsPerJob * numJobs
config.Data.publication = False

#config.Site.whitelist = [ siteName ]
config.Site.storageSite = 'T2_IT_Legnaro'

# In case you want to send jobs to the ITB pool
#config.Debug.scheddName = 'crab3@vocms069.cern.ch'
#config.Debug.collector = 'cmsgwms-collector-itb.cern.ch'

config.Debug.extraJDL = ['+DESIRED_Sites="%s"' % siteName, '+SingularityImage="/cvmfs/singularity.opensciencegrid.org/cmssw/cms:rhel%s-itb"' % arch[3]]
