#!/bin/sh
alias cmsrel='scramv1 project CMSSW'
alias cmsenv='eval `scramv1 runtime -sh`'

# Check if the CMSSW releases are there, otherwise create them
[[ -d CMSSW_10_6_15 ]] || SCRAM_ARCH=slc7_amd64_gcc700 cmsrel CMSSW_10_6_15
[[ -d CMSSW_9_4_17 ]] || SCRAM_ARCH=slc6_amd64_gcc630 cmsrel CMSSW_9_4_17

for CMSSW_RELEASE in {CMSSW_9_4_17,CMSSW_10_6_15}; do
  cd $CMSSW_RELEASE
  cmsenv
  cd ..
  for SITE in {'T1_DE_KIT','T1_US_FNAL','T1_IT_CNAF','T2_DE_DESY','T2_US_Caltech'}; do
    export SITE
    crab-prod submit crabTask.py
  done
done

echo
echo
echo "Please, check this link to monitor the progresses of the tasks: https://monit-grafana.cern.ch/d/cmsTMGlobal/cms-tasks-monitoring-globalview?orgId=11&var-user=`whoami`&var-site=All&var-current_url=%2Fd%2FcmsTMDetail%2Fcms_task_monitoring&var-task=All"
