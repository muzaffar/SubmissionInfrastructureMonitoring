#!/bin/bash

POOL=$1
# Adding check to ensure script runs only once in 12min interval

my12min=`/bin/date +"%s / 720" | /usr/bin/bc`
echo my12min
if [ -e /tmp/Monitor_${POOL}_cron.${my12min} ]; then
   echo "Already executed for this 12 min bin"
   exit 0
else
   /bin/rm -f /tmp/Monitor_${POOL}_cron.*
fi
# Now running MonIT script

# cd into the directory where the script is so we can use relative paths
script_path=$(dirname $(readlink -f $0))
cd $script_path

# Pick up the common StompAMQ module
CMSMonitoring="/data/srv/SubmissionInfrastructureMonitoring/CMSMonitoring/src/python"
if [ -z $PYTHONPATH ] ; then
  export PYTHONPATH=$CMSMonitoring
else
  export PYTHONPATH=${PYTHONPATH}:$CMSMonitoring
fi

# Monitor the itb pool
python my_monitor.py $POOL push 1>> log/$POOL/log.out 2>> log/$POOL/log.err

/bin/touch /tmp/Monitor_${POOL}_cron.${my12min}
exit 0
