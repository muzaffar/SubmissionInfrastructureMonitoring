"""
This module collects data for factories monitoring and sends it to MONIT
"""
from __future__ import division
from __future__ import print_function
import os
import ssl
import sys
import json
import http.client as httplib
import time
import logging
import traceback
from collections import OrderedDict
from urllib import request
from optparse import OptionParser
from logging import handlers
import configparser
import xmltodict
import requests
from CMSMonitoring.StompAMQ import StompAMQ


def get_data(url):
    """
    Retrieve an XML file from a URL and convert data to dictionary
    """
    url = "http://" + url
    logger.info("Will try to get data from %s", url)
    response = requests.get(url)
    if response.status_code != 200:
        logger.error("Error %s getting information from %s", response.status_code, url)
        return {}

    data = xmltodict.parse(response.content)
    if not data:
        logger.error("No data from %s", url)

    return data


def get_descript_dictionary(descript_data):
    """
    Get specific attributes from descript data
    """
    data = {}

    entries = descript_data.get("glideFactoryDescript", {}).get("entries", {}).get("entry", [])
    for entry in entries:
        entry_name = entry.get("@name")
        if not entry_name:
            logger.warning("Descript data entry has no name %s", entry)
            continue
        logger.info("Processing descript data for %s", entry_name)

        attributes = entry.get("attributes")
        if not attributes:
            logger.warning("Descript data entry has no attributes %s", entry)
            continue

        descript = entry.get("descript")
        if not attributes:
            logger.warning("Descript data entry has no descript %s", entry)
            continue

        data[entry_name] = {
            "GLIDEIN_CMSSite": attributes.get("@GLIDEIN_CMSSite", "Unknown"),
            "GLIDEIN_CPUS": attributes.get("@GLIDEIN_CPUS", 0),
            "GLIDEIN_ESTIMATED_CPUS": attributes.get("@GLIDEIN_ESTIMATED_CPUS", 0),
            "GLIDEIN_Site": attributes.get("@GLIDEIN_Site", "Unknown"),
            "GLIDEIN_ResourceName": attributes.get("@GLIDEIN_ResourceName", "Unknown"),
            "DefaultPerFrontendMaxGlideins": int(descript.get("@DefaultPerFrontendMaxGlideins", 0)),
            "DefaultPerFrontendMaxHeld": int(descript.get("@DefaultPerFrontendMaxHeld", 0)),
            "DefaultPerFrontendMaxIdle": int(descript.get("@DefaultPerFrontendMaxIdle", 0)),
            "PerEntryMaxGlideins": int(descript.get("@PerEntryMaxGlideins", 0)),
            "PerEntryMaxHeld": int(descript.get("@PerEntryMaxHeld", 0)),
            "PerEntryMaxIdle": int(descript.get("@PerEntryMaxIdle", 0)),
        }

    return data


def get_completed_dictionary(completed_data, period_name):
    """
    Get specific attributes from completed data for selected period
    """
    data = {}
    if not completed_data:
        logger.warning("No completed data")
        return data

    entries = completed_data.get("glideFactoryRRDStats", {}).get("entries", {}).get("entry", [])
    for entry in entries:
        entry_name = entry.get("@name")
        if not entry_name:
            logger.warning("Completed data entry has no name %s", entry)
            continue
        logger.info("Processing completed data for %s", entry_name)

        frontends = entry.get("frontends")
        if not frontends:
            logger.warning("Completed data entry has no frontends %s", entry)
            continue

        frontend_elements = frontends.get("frontend", [])
        if isinstance(frontend_elements, OrderedDict):
            frontend_elements = [frontend_elements]

        for frontend_element in frontend_elements:
            frontend_element_name = frontend_element.get("@name")
            if not frontend_element_name:
                logger.warning("Completed data frontend element has no name %s", frontend_element)
                continue

            for period in frontend_element.get("periods", {}).get("period", []):
                if int(period.get("@name")) == period_name:
                    data[(entry_name, frontend_element_name)] = {
                        "FailedNr": float(period.get("@FailedNr", 0)),
                        "Glideins": float(period.get("@Glideins", 0)),
                        "Lasted": float(period.get("@Lasted", 0)),
                    }

    return data


def get_waste_time_dictionary(waste_time_data, period_name):
    """
    Get specific attributes from waste time data for selected period
    """
    data = {}
    if not waste_time_data:
        logger.warning("No waste time data")
        return data

    entries = waste_time_data.get("glideFactoryRRDStats", {}).get("entries", {}).get("entry", [])
    for entry in entries:
        entry_name = entry.get("@name")
        if not entry_name:
            logger.warning("Waste time data entry has no name %s", entry)
            continue
        logger.info("Processing waste time data for %s", entry_name)

        frontends = entry.get("frontends")
        if not frontends:
            logger.warning("Waste time data entry has no frontends %s", entry)
            continue

        frontend_elements = frontends.get("frontend", [])
        if isinstance(frontend_elements, OrderedDict):
            frontend_elements = [frontend_elements]

        for frontend_element in frontend_elements:
            frontend_element_name = frontend_element.get("@name")
            if not frontend_element_name:
                logger.warning("Waste time data frontend element has no name %s", frontend_element)
                continue

            for period in frontend_element.get("periods", {}).get("period", []):
                if int(period.get("@name")) == period_name:
                    data[(entry_name, frontend_element_name)] = {
                        "validation_All": float(period.get("@validation_All", 0)),
                    }

    return data


def get_job_duration_dictionary(job_duration_data, period_name):
    """
    Get specific attributes from job duration data for selected period
    """
    data = {}
    if not job_duration_data:
        logger.warning("No job duration data")
        return data

    entries = job_duration_data.get("glideFactoryRRDStats", {}).get("entries", {}).get("entry", [])
    for entry in entries:
        entry_name = entry.get("@name")
        if not entry_name:
            logger.warning("Job duration data entry has no name %s", entry)
            continue
        logger.info("Processing job duration data for %s", entry_name)

        frontends = entry.get("frontends")
        if not frontends:
            logger.warning("Job duration data entry has no frontends %s", entry)
            continue

        frontend_elements = frontends.get("frontend", [])
        if isinstance(frontend_elements, OrderedDict):
            frontend_elements = [frontend_elements]

        for frontend_element in frontend_elements:
            frontend_element_name = frontend_element.get("@name")
            if not frontend_element_name:
                logger.warning("Job duration data frontend element has no name %s",
                               frontend_element)
                continue

            for period in frontend_element.get("periods", {}).get("period", []):
                if int(period.get("@name")) == period_name:
                    data[(entry_name, frontend_element_name)] = {
                        "JobsNr_None": float(period.get("@JobsNr_None", 0))
                    }

    return data


def get_schedd_dictionary(schedd_data):
    """
    Get specific attributes from scedd data
    """
    data = {}
    if not schedd_data:
        return data

    entries = schedd_data.get("glideFactoryQStats", {}).get("entries", {}).get("entry", [])
    for entry in entries:
        entry_name = entry.get("@name")
        if not entry_name:
            logger.warning("Schedd data data entry has no name %s", entry)
            continue
        logger.info("Processing schedd data for %s", entry_name)

        frontends = entry.get("frontends")
        if not frontends:
            logger.warning("Schedd data entry has no frontends %s", entry)
            continue

        downtime = entry.get("downtime", {})
        downtime_status = downtime.get("@status", "False")
        if downtime_status == "True":
            downtime_status = 1
        else:
            downtime_status = 0

        logger.info("Processing schedd data for %s", entry_name)
        frontend_elements = frontends.get("frontend", [])
        if isinstance(frontend_elements, OrderedDict):
            frontend_elements = [frontend_elements]

        for frontend_element in frontend_elements:
            frontend_element_name = frontend_element.get("@name")
            if not frontend_element_name:
                logger.warning("Schedd data frontend element has no name %s", frontend_element)
                continue

            client_monitor = frontend_element.get("ClientMonitor", {})
            requested = frontend_element.get("Requested", {})
            status = frontend_element.get("Status", {})
            data[(entry_name, "frontend_" + frontend_element_name)] = {
                "ClientMonitor_CoresIdle": int(client_monitor.get("@CoresIdle", 0)),
                "ClientMonitor_CoresRunning": int(client_monitor.get("@CoresRunning", 0)),
                "ClientMonitor_CoresTotal": int(client_monitor.get("@CoresTotal", 0)),
                "ClientMonitor_GlideIdle": int(client_monitor.get("@GlideIdle", 0)),
                "ClientMonitor_GlideRunning": int(client_monitor.get("@GlideRunning", 0)),
                "ClientMonitor_GlideTotal": int(client_monitor.get("@GlideTotal", 0)),
                "ClientMonitor_JobsIdle": int(client_monitor.get("@JobsIdle", 0)),
                "ClientMonitor_JobsRunHere": int(client_monitor.get("@JobsRunHere", 0)),
                "ClientMonitor_JobsRunning": int(client_monitor.get("@JobsRunning", 0)),
                "Requested_Idle": int(requested.get("@Idle", 0)),
                "Requested_IdleCores": int(requested.get("@IdleCores", 0)),
                "Status_Held": int(status.get("@Held", 0)),
                "Status_Idle": int(status.get("@Idle", 0)),
                "Status_IdleOther": int(status.get("@IdleOther", 0)),
                "Status_Pending": int(status.get("@Pending", 0)),
                "Status_Running": int(status.get("@Running", 0)),
                "Status_RunningCores": int(status.get("@RunningCores", 0)),
                "Status_Wait": int(status.get("@Wait", 0)),
                "Downtime_status": downtime_status,
                }

    return data


def get_alerts_exceptions_dictionary(alerts_exceptions_file):
    """
    Get entries that should be excluded from alerts
    """
    exceptions = {}
    if os.path.isfile(alerts_exceptions_file):
        with open (alerts_exceptions_file, "r") as fileHandler:
            for line in fileHandler:
                line = line.strip()
                if line != "\n" or not line.startswith("#"):
                    fields = line.split(" ", 1)
                    if len(fields) > 1:
                        exceptions[fields[0]] = {"comment": fields[1]}
                    else:
                        exceptions[fields[0]] = {"comment": "Unknown"}
    else:
        logger.error(alerts_exceptions_file + " file does not exist")

    return exceptions


def create_documents(completed_dictionary,
                     waste_time_dictionary,
                     job_duration_dictionary,
                     schedd_dictionary,
                     descript_dictionary,
                     alerts_exceptions_dictionary,
                     frontends,
                     cms_frontends,
                     suspended_sites):
    """
    Create documents from all collected data
    """
    data_for_pilot = []
    data_for_entry = []
    keys = set(completed_dictionary.keys())
    keys.update(waste_time_dictionary.keys())
    keys.update(job_duration_dictionary.keys())
    keys.update(schedd_dictionary.keys())
    missing_frontends = set()
    for key in keys:
        entry, frontend = key
        completed_value = completed_dictionary.get(key, {})
        waste_time_value = waste_time_dictionary.get(key, {})
        job_duration_value = job_duration_dictionary.get(key, {})
        schedd_value = schedd_dictionary.get(key, {})
        descript_value = descript_dictionary.get(entry, {})
        downtime_status = schedd_value.get("Downtime_status", 0)
        alerts_exceptions_value = alerts_exceptions_dictionary.get(entry, {})
        if alerts_exceptions_value:
            alert = 0
            alert_comment = alerts_exceptions_value.get("comment", "Unknown")
        else:
            if downtime_status:
                alert = 0
                alert_comment = "Downtime"
            elif get_resource(descript_value) in suspended_sites:
                alert = 0
                alert_comment = "Suspended site"
            else:
                alert = 1
                alert_comment = "Send alerts"
        glideins = completed_value.get("Glideins", 0)
        lasted = completed_value.get("Lasted", 0)
        logger.info("Creating document for %s in %s", entry, frontend)
        if frontend in frontends:
            frontend = frontends[frontend]
        else:
            missing_frontends.add(frontend)

        site = get_site(descript_value, frontend, cms_frontends)

        data_for_pilot.append({
            "entry": entry,
            "frontend": frontend,
            "Site": site,
            "CondorFailedToStart": completed_value.get("FailedNr", 0) / glideins * 100 if glideins else 0,
            "ValidationFailed": waste_time_value.get("validation_All", 0) / lasted * 100 if lasted else 0,
            "PilotsRun0Jobs": job_duration_value.get("JobsNr_None", 0) / glideins * 100 if glideins else 0,
            "ClientMonitor_CoresIdle": schedd_value.get("ClientMonitor_CoresIdle", 0),
            "ClientMonitor_CoresRunning": schedd_value.get("ClientMonitor_CoresRunning", 0),
            "ClientMonitor_CoresTotal": schedd_value.get("ClientMonitor_CoresTotal", 0),
            "ClientMonitor_GlideIdle": schedd_value.get("ClientMonitor_GlideIdle", 0),
            "ClientMonitor_GlideRunning": schedd_value.get("ClientMonitor_GlideRunning", 0),
            "ClientMonitor_GlideTotal": schedd_value.get("ClientMonitor_GlideTotal", 0),
            "ClientMonitor_JobsIdle": schedd_value.get("ClientMonitor_JobsIdle", 0),
            "ClientMonitor_JobsRunHere": schedd_value.get("ClientMonitor_JobsRunHere", 0),
            "ClientMonitor_JobsRunning": schedd_value.get("ClientMonitor_JobsRunning", 0),
            "Requested_Idle": schedd_value.get("Requested_Idle", 0),
            "Requested_IdleCores": schedd_value.get("Requested_IdleCores", 0),
            "Status_Held": schedd_value.get("Status_Held", 0),
            "Status_Idle": schedd_value.get("Status_Idle", 0),
            "Status_IdleOther": schedd_value.get("Status_IdleOther", 0),
            "Status_Pending": schedd_value.get("Status_Pending", 0),
            "Status_Running": schedd_value.get("Status_Running", 0),
            "Status_RunningCores": schedd_value.get("Status_RunningCores", 0),
            "Status_Wait": schedd_value.get("Status_Wait", 0),
            "Downtime_status": downtime_status,
            "Cores": get_cores(descript_value.get("GLIDEIN_CPUS", 0), descript_value.get("GLIDEIN_ESTIMATED_CPUS", 0)),
            "Limit_per_frontend_held": descript_value.get("DefaultPerFrontendMaxHeld", 0),
            "Limit_per_frontend_idle": descript_value.get("DefaultPerFrontendMaxIdle", 0),
            "Limit_per_frontend_total": descript_value.get("DefaultPerFrontendMaxGlideins", 0),
            "Alert": alert,
        })

        for element in data_for_entry:
            if entry == element["entry"]:
                element["frontends"].append(frontend)
                element["frontends_number"] += 1
                if site not in element["Site_names"]:
                    element["Site_names"].append(site)
                break
        else:
            data_for_entry.append({
                "entry": entry,
                "frontends": [frontend],
                "frontends_number": 1,
                "Site_names": [site],
                "Limit_per_entry_held": descript_value.get("PerEntryMaxHeld"),
                "Limit_per_entry_idle": descript_value.get("PerEntryMaxIdle"),
                "Limit_per_entry_total": descript_value.get("PerEntryMaxGlideins"),
                "Alert": alert,
                "Alert_comment": alert_comment,
            })

    for frontend in missing_frontends:
        logger.warning("%s frontend is not in frontends dictionary: %s", frontend, frontends)

    return data_for_pilot, data_for_entry


def get_amq(host, port, producer, topic, ckey, cert):
    """
    Create StompAMQ object
    """
    username = ""
    password = ""
    amq = StompAMQ(username,
                   password,
                   producer,
                   topic,
                   key=ckey,
                   cert=cert,
                   validation_schema=None,
                   host_and_ports=[(host, port)])

    return amq


def create_notification_documents(amq, documents, document_type, factory):
    """
    Create notification documents
    """
    seconds = str(int(time.time()))
    data = []
    for document in documents:
        frontend = document.get("frontend")
        if frontend:
            document_id = "%s_%s_%s_%s" % (document["entry"],
                                           frontend.replace(" ", "_"),
                                           factory,
                                           seconds)
        else:
            document_id = "%s_%s_%s" % (document["entry"],
                                        factory,
                                        seconds)
        notification, _, _ = amq.make_notification(document,
                                                   document_type,
                                                   docId=document_id,
                                                   metadata={"factory": factory,
                                                             "type": document_type},
                                                   dataSubfield=None)
        data.append(notification)

    return data


def get_site(entry_data, frontend, cms_frontends):
    """
    Get GLIDEIN_CMSSite value for CMS frontends and GLIDEIN_Site value for non-CMS frontends
    """
    attribute = "GLIDEIN_Site"
    if frontend in cms_frontends:
        attribute = "GLIDEIN_CMSSite"
    site = entry_data.get(attribute, "Unknown")

    return site


def get_resource(entry_data):
    """
    Get GLIDEIN_ResourceName value
    """

    return entry_data.get("GLIDEIN_ResourceName", "Unknown")


def get_cores(cpus, estimated_cpus):
    """
    Get cores from GLIDEIN_CPUS and GLIDEIN_ESTIMATED_CPUS
    """
    try:
        cores = int(cpus)
    except ValueError:
        try:
            cores = int(estimated_cpus)
        except ValueError:
            cores = 0

    return cores


def setup_logging():
    """
    Setup the logger
    """
    main_logger = logging.getLogger("logger")
    main_logger.setLevel(logging.INFO)
    log_format = "[%(asctime)s][%(levelname)s] %(message)s"
    formatter = logging.Formatter(fmt=log_format, datefmt="%Y-%m-%d %H:%M:%S")
    console = logging.StreamHandler()
    main_logger.addHandler(console)
    main_logger.propagate = False
    console.setFormatter(formatter)

    sys.stdout = LoggerWriter(main_logger.info)
    sys.stderr = LoggerWriter(main_logger.error)

    return main_logger


class LoggerWriter(object):
    """
    Wrapper class to redirect write function to a given function
    """
    def __init__(self, level):
        self.level = level

    def write(self, message):
        """
        If message is not only newline,
        rstrip and pass it to given function
        """
        if message != "\n":
            self.level(message.rstrip())

    def flush(self):
        pass


def add_file_logging(log_file_name):
    """
    Log to file
    """
    # Max log file size - 10Mb
    max_log_file_size = 1024 * 1024 * 10
    max_log_file_count = 10
    log_directory = "/".join(log_file_name.split("/")[:-1])
    if log_directory and not os.path.exists(log_directory):
        os.makedirs(log_directory)

    handler = handlers.RotatingFileHandler(log_file_name,
                                           "a",
                                           max_log_file_size,
                                           max_log_file_count)

    formatter = logger.handlers[0].formatter
    handler.setFormatter(formatter)
    logger.addHandler(handler)


def get_suspended_sites(url):
    """
    Get suspended sites
    """
    opener = request.build_opener(X509CertOpen())
    datareq = request.Request(url)
    response = opener.open(datareq).read()
    resources = xmltodict.parse(response)
    suspended_sites = []
    for resource in resources.get("results", {}).get("SITE", {}):
        if resource.get("CERTIFICATION_STATUS", "") == "Suspended":
            suspended_sites.append(resource["@NAME"])

    return suspended_sites


class X509CertAuth(httplib.HTTPSConnection):

    def __init__(self, host, *args, **kwargs):
        key_file = ckey
        cert_file = cert

        if not os.path.exists(key_file):
            print("No certificate private key file found")
            exit(1)

        if not os.path.exists(cert_file):
            print("No certificate public key file found")
            exit(1)

        httplib.HTTPSConnection.__init__(self, host, key_file=key_file, cert_file=cert_file,
                                         context=ssl._create_unverified_context(), **kwargs)

class X509CertOpen(request.AbstractHTTPHandler):

    def default_open(self, req):
        return self.do_open(X509CertAuth, req)


def main():
    """
    Collect data for factories monitoring and send it to MONIT
    """
    parser = OptionParser()
    parser.add_option("--send",
                      action="store_true",
                      default=False,
                      dest="send",
                      help="If present, send created documents to selected destination. "
                           "If absent, print created documents and do not send them to "
                           "selected destination.")
    parser.add_option("--quiet",
                      action="store_true",
                      default=False,
                      dest="quiet",
                      help="Silent or quiet mode. Do not print logger messages.")
    parser.add_option("--log_to_file",
                      default="",
                      dest="log_to_file",
                      help="Print logs to file")
    destination_choices = ("training", "testing", "production")
    parser.add_option("--destination",
                      type="choice",
                      choices=destination_choices,
                      default=destination_choices[0],
                      dest="destination",
                      help="Send created documents to selected destination. Valid choices are %s. "
                           "Default: %s" % (destination_choices, destination_choices[0]))
    parser.add_option("--config",
                      default="",
                      dest="config_name",
                      help="Get information from configuration file")
    (options, _) = parser.parse_args()
    log_file_name = options.log_to_file
    if log_file_name:
        add_file_logging(log_file_name)

    if options.quiet:
        logger.disabled = True

    logger.info("Started")
    config_name = options.config_name
    config = configparser.ConfigParser(interpolation=configparser.ExtendedInterpolation())
    config.optionxform = str
    if os.path.isfile(config_name):
        config.read(config_name)
    else:
        logger.error(config_name + " file does not exist")
        return
    destination = options.destination
    logger.info("Selected destination is %s", destination)
    try:
        producer = config.get(destination, "producer")
        topic = config.get(destination, "topic")
        global ckey
        global cert
        ckey = config.get(destination, "ckey")
        cert = config.get(destination, "cert")
        document_type_for_pilot = config.get(destination, "document_type_for_pilot")
        document_type_for_entry = config.get(destination, "document_type_for_entry")
        host = str(config.get(destination, "host"))
        port = str(config.get(destination, "port"))
        alerts_exceptions_file = config.get(destination, "alerts_exceptions_file")
        factories = {x.rsplit(":", 1)[0].strip(): x.rsplit(":", 1)[1].strip()
                     for x in config.get(destination, "factories").split("\n")}
        frontends = {x.rsplit(":", 1)[0].strip(): x.rsplit(":", 1)[1].strip()
                     for x in config.get(destination, "frontends").split("\n")}
        cms_frontends = [x.strip() for x in config.get(destination, "cms_frontends").split("\n")]
        period = config.getint(destination, "period")
        completed_url_path = config.get(destination, "completed_url_path")
        waste_time_url_path = config.get(destination, "waste_time_url_path")
        job_duration_url_path = config.get(destination, "job_duration_url_path")
        schedd_url_path = config.get(destination, "schedd_url_path")
        descript_url_path = config.get(destination, "descript_url_path")
        gocdb_url = config.get(destination, "gocdb_url")
    except (configparser.NoSectionError, configparser.NoOptionError) as ex:
        logger.error(ex)
        return
    suspended_sites = get_suspended_sites(gocdb_url)
    amq = get_amq(host, port, producer, topic, ckey, cert)
    for factory_host, factory_name in factories.items():
        logger.info("Processing %s", factory_host)
        descript_data = get_data(factory_host + descript_url_path)
        if not descript_data:
            logger.warning("There is no information from %s", factory_host + descript_url_path)
            continue
        completed_data = get_data(factory_host + completed_url_path)
        waste_time_data = get_data(factory_host + waste_time_url_path)
        job_duration_data = get_data(factory_host + job_duration_url_path)
        schedd_data = get_data(factory_host + schedd_url_path)
        if (not completed_data and not waste_time_data
                and not job_duration_data and not schedd_data):
            logger.warning("There is not any information from %s factory XML files.", factory_name)
            continue
        descript_dictionary = get_descript_dictionary(descript_data)
        completed_dictionary = get_completed_dictionary(completed_data, period)
        waste_time_dictionary = get_waste_time_dictionary(waste_time_data, period)
        job_duration_dictionary = get_job_duration_dictionary(job_duration_data, period)
        schedd_dictionary = get_schedd_dictionary(schedd_data)
        alerts_exceptions_dictionary = get_alerts_exceptions_dictionary(alerts_exceptions_file)
        documents_for_pilot, documents_for_entry = create_documents(completed_dictionary,
                                                                    waste_time_dictionary,
                                                                    job_duration_dictionary,
                                                                    schedd_dictionary,
                                                                    descript_dictionary,
                                                                    alerts_exceptions_dictionary,
                                                                    frontends,
                                                                    cms_frontends,
                                                                    suspended_sites)
        data_for_pilot = create_notification_documents(amq,
                                                       documents_for_pilot,
                                                       document_type_for_pilot,
                                                       factory_name)
        data_for_entry = create_notification_documents(amq,
                                                       documents_for_entry,
                                                       document_type_for_entry,
                                                       factory_name)
        if not options.send:
            logger.info(json.dumps(data_for_pilot, indent=4))
            logger.info(json.dumps(data_for_entry, indent=4))
        else:
            logger.info("Will send created documents to %s monitoring", destination)
            # Sleep prevents error [Errno 32] Broken pipe
            time.sleep(3)
            amq.send(data_for_pilot)
            time.sleep(3)
            amq.send(data_for_entry)
    logger.info("Finished")


if __name__ == "__main__":
    logger = setup_logging()
    try:
        main()
    except Exception as ex:
        logger.error(ex)
        logger.error(traceback.format_exc())
