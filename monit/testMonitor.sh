#!/bin/bash
export CMS_MONIT_ENDPOINT='cms-test-mb.cern.ch'
export CMS_MONIT_PRODUCER='cms-test'

POOL=$1
# Adding check to ensure script runs only once in 12min interval

my12min=`/bin/date +"%s / 720" | /usr/bin/bc`
echo my12min
if [ -e /tmp/Monitor_test_$POOL_cron.${my12min} ]; then
   echo "Already executed for this 12 min bin"
   exit 0
else
   /bin/rm -f /tmp/Monitor_test_$POOL_cron.*
fi
# Now running MonIT script

# cd into the directory where the script is so we can use relative paths
script_path=$(dirname $(readlink -f $0))
cd $script_path

# cd into the directory where the script is so we can use relative paths
script_path=$(dirname "$(readlink -f "$0")")
cd "$script_path"
LOG_DIR="$script_path/log_test"
mkdir -p "$LOG_DIR/"{itb,global,cern,volunteer}

# Pick up the common StompAMQ module
CMSMonitoring="/data/srv/SubmissionInfrastructureMonitoring/CMSMonitoring/src/python"
if [ -z $PYTHONPATH ] ; then
  export PYTHONPATH=$CMSMonitoring
else
  export PYTHONPATH=${PYTHONPATH}:$CMSMonitoring
fi

# Monitor the itb pool
python my_monitor.py $POOL push 1>> $LOG_DIR/$POOL/log.out 2>> log/$POOL/log.err

/bin/touch /tmp/Monitor_test_$POOL_cron.${my12min}
exit 0
