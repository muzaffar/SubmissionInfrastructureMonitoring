# ***************************************************
# GlideinWMS Frontend statistics monitoring.
# It parses XML file, convert data to JSON and Publish
# ****************************************************
from __future__ import division
from __future__ import print_function
import os
import sys
import json
import time
import requests
import logging
import traceback
from collections import OrderedDict
from optparse import OptionParser
from logging import handlers
import xmltodict
import configparser
import glob
from CMSMonitoring.StompAMQ import StompAMQ



def get_data(url):
    """
    Retrieve an XML file from a URL and convert data to dictionary
    """
    url = "http://" + url
    log.info("Will try to get data from %s", url)
    response = requests.get(url)
    if response.status_code != 200:
        log.error("Error %s getting information from %s", response.status_code, url)
        print ("Probem fetching data")
        return None

    data = xmltodict.parse(response.content,dict_constructor=dict)
    if not data:
        log.error("No data from %s", url)

    return data


def parse_fe_xml(host,amq,pool):
    """
    Parse XML file of frontend host
    """
    
    output_action = "push"
    timestamp=int(time.time()*1000)
    dict_add =[]
    #HOST="vocms0819.cern.ch"
    HOST=host
    Mydata =get_data(HOST+"/vofrontend/monitor/frontend_status.xml")

    #obj = Mydata["VOFrontendGroupStats"]
    obj = Mydata["VOFrontendStats"]

    #for element in obj['factories']['factory']:
    #    print (element.get('MatchedCores'))
    for element in obj['groups']['group']:
        group = element.get('@name')
        groupstats =get_data(HOST+"/vofrontend/monitor/group_"+group+"/frontend_status.xml")
        if groupstats:

            VOFrontendGroupStats = groupstats["VOFrontendGroupStats"]
            
            # Do not pare FE group xml file, if timestamp "updated" is unchanged, in new ietration.
      
            update =  VOFrontendGroupStats.get('updated')
            update_time =  update.get('timezone')
            human_time=update_time[0].get('@ISO8601')
            tmp_file="/tmp/fegroup_"+host+"_"+group+"_"+human_time
            tmp_file_pattern_list=glob.glob("/tmp/fegroup_"+host+"_"+group+"*")

            if not os.path.exists(tmp_file):
                for filePath in tmp_file_pattern_list:
                    try:
                        os.remove(filePath)
                    except OSError:
                        log.error("Error while deleting file")
                with open(tmp_file, 'w'): pass

                log.info("file created %s",tmp_file)

                factories= VOFrontendGroupStats.get('factories')
                if  factories:
                    for tag in VOFrontendGroupStats['factories']['factory']:

                        Attributes =tag.get('Attributes')
                        if not Attributes:
                            continue
                        Requested =tag.get('Requested')
                        if not Requested:
                            continue

                        if Attributes.get('@GLIDEIN_CPUS') == "auto":
                            GLIDEIN_CPUS = Attributes.get('@GLIDEIN_ESTIMATED_CPUS',0)
                        else:
                            GLIDEIN_CPUS = Attributes.get('@GLIDEIN_CPUS',0)

                        pilots_requested = Requested.get('@Idle',0)
                        Requested_Cores = int(pilots_requested) * int(GLIDEIN_CPUS)



                        Name = tag.get('@name')


                        Request_Factory = Name.split("@")[-1]
                        FE_Data = {
                        "FE_Group" : group,
                        "Request_Factory" : Request_Factory,
                        "GlideinWMSVersion" : Attributes.get('@GlideinWMSVersion', "unknown"),
                        "FactoryName" : Attributes.get('@FactoryName', "unknown"),
                        "CMSSite" : Attributes.get('@GLIDEIN_CMSSite', "unknown"),
                        "Entry" : Attributes.get('@EntryName', "unknown"),
                        "GLIDEIN_CPUS" : GLIDEIN_CPUS ,
                        "Requested_Idle" : Requested.get('@Idle',0),
                        "Requested_Cores" : Requested_Cores,
                        }

                        #dict_add = json.loads(json.dumps(FE_Data))

                        #print (json_data)
                        dict_add.append(json.loads(json.dumps(FE_Data)))

            else:
                log.info("%s group is not updated on FE %s",group,HOST)
                continue

        metadata={'timestamp' : timestamp,
              'producer' : "cms",
              'type' : "si_frontend",
              'type_prefix' : "raw",
              'version' : "0.1",
              'pool' : pool,
              'frontend_host' : host}

    doc_type = 'si_frontend'
    #doc_type = 'raw_metric'
    notifications = []
    for payload in dict_add:
        ndoc, _, _ = amq.make_notification(payload, doc_type, dataSubfield='payload', metadata=metadata)
        notifications.append(ndoc)
    # Print the collected data
    if output_action == "both" or output_action == "print":
        for notification in notifications:
            print(json.dumps(notification, sort_keys=True, indent=4))
    # Push the collected data
    if output_action == "both" or output_action == "push":
        amq.send(notifications)


    #return dict_add                






########################################################
# Setup the logger and the log level {CRITICAL, ERROR, WARNING, INFO, DEBUG, NOTSET}
logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(name)s -  %(levelname)s - %(message)s', datefmt='%d-%m-%y %H:%M:%S')
log = logging.getLogger(__name__)

# Monit specific configuration
monit_end_point=[(os.getenv("CMS_MONIT_ENDPOINT", 'cms-mb.cern.ch'), 61323)]
monit_producer=os.getenv("CMS_MONIT_PRODUCER","cms")
monit_topic="/topic/cms.si.condor"
# username and passwrod aren't necessary when using certificates
monit_username=""
monit_password=""

# Certificates used to push data through the AMQ service
my_cert="/etc/grid-security/hostcert.pem"
my_key="/etc/grid-security/hostkey.pem"


    
amq=StompAMQ(monit_username, monit_password, monit_producer, monit_topic, validation_schema=None, host_and_ports=monit_end_point, logger=log, cert=my_cert, key=my_key)

cern_fe = "vocms0819.cern.ch"
itb_fe =  "vocms0802.cern.ch"
global_fe =  "vocms080.cern.ch"
#global_fe_fnal =  "okd-cmsglobal-fe01.fnal.gov:8319"
global_fe_fnal =  "cmssrvz05.fnal.gov:8319"
DATA =[]
DATA = parse_fe_xml(cern_fe, amq,"cern")
DATA = parse_fe_xml(global_fe, amq,"global")
DATA = parse_fe_xml(global_fe_fnal, amq,"global")
DATA = parse_fe_xml(itb_fe, amq,"itb")
